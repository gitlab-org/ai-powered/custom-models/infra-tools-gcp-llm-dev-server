const { Firestore } = require('@google-cloud/firestore');
const httpProxy = require('http-proxy');
const { google } = require('googleapis');
const compute = google.compute('v1');
const streamify = require('stream-array');

const firestore = new Firestore();
const proxy = httpProxy.createProxyServer({});
let targetUrl = process.env.TARGET_URL;
const logsCollectionId = process.env.COLLECTION_ID;
const keysCollectionId = process.env.KEYS_COLLECTION_ID;
const projectId = process.env.PROJECT_ID;
const zone = process.env.ZONE;
const instanceName = process.env.INSTANCE_NAME;
const instanceStartDuration = parseInt(process.env.INSTANCE_START_DURATION,10);

if (!/^https?:\/\//i.test(targetUrl)) {
  targetUrl = `http://${targetUrl}`;
}

// Function to restart the GCE instance if it is stopped or terminated
async function restartInstanceIfNeeded() {
    // Authorize the client with application default credentials
    const authClient = await google.auth.getClient({
      scopes: ['https://www.googleapis.com/auth/cloud-platform']
    });
    google.options({ auth: authClient });

    // Check the status of the instance
    const instance = await compute.instances.get({
      project: projectId,
      zone: zone,
      instance: instanceName
    });

    if (instance.data.status === 'TERMINATED' || instance.data.status === 'STOPPED') {
      // Start the GCE instance if it is stopped or terminated
      const startResponse = await compute.instances.start({
        project: projectId,
        zone: zone,
        instance: instanceName
      });

      console.log(`Instance ${instanceName} started successfully`, startResponse.data);

      // Wait for the instance to start
      await new Promise(resolve => setTimeout(resolve, instanceStartDuration)); // wait for 1 minute
    }
}


async function authorizeRequest(req, res) {
    const header = req.headers?.authorization?.split(' ') || [];
    const [method, apiKey] = header;
    const badAuthorization = { valid: false, userName: 'UNAUTHORIZED' }

    if(method !== 'Bearer' || !apiKey) {
        return badAuthorization;
    }

    const userCollection = firestore.collection(keysCollectionId);

    const keyDocs = await userCollection.where('apiKey', '==', apiKey).get();

    if(keyDocs.empty) {
        return badAuthorization;
    }

    const key = keyDocs.docs[0].data();

    return { valid: true, userName: key.userName }
}

async function logRequest(req, res, authorization){
    const logDocRef = firestore.collection(logsCollectionId).doc();
    const timestamp = new Date().toISOString();
    const createdAt = new Date();
    const requestData = { timestamp: timestamp, created_at: createdAt, authorization: authorization };

    if (req.method) {
        requestData.method = req.method;
    }
    if (req.headers) {
        requestData.headers = req.headers;
    }
    if (req.url) {
        requestData.path = req.url;
    }
    try {
     await logDocRef.set(requestData)
    } catch (error) {
        console.error('Error while logging the request request:', error);
        res.status(500).send('Logging error: ' + err.message);
    }
}

exports.handler = async (req, res) => {
  try {
    const authorization = await authorizeRequest(req, res);
    await logRequest(req, res, authorization);

    if (authorization.valid === false) {
        res.status(401).send('Unauthorized Request');
        return;
    }

    await restartInstanceIfNeeded();

    const proxyParams = { target: targetUrl }
    if (req.rawBody) {
      proxyParams.buffer = streamify([req.rawBody]);
    }

    proxy.web(req, res, proxyParams, (err) => {
      if (err) {
        res.status(500).send('Proxy error: ' + err.message);
      }
    });
  } catch (error) {
    console.error('Error restarting instance or proxying request:', error);
    res.status(500).send('Error restarting instance or proxying request');
  }
};

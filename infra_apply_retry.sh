if sh zip_fonction.sh && terraform plan; then
  max_retries=10
  retry_count=0

  # Function to handle SIGINT (Ctrl+C)
  function handle_interrupt {
      echo " ❌ Script interrupted by user. Exiting..."
      exit 1
  }

  # Trap SIGINT and call handle_interrupt
  trap handle_interrupt SIGINT

  # Loop until the command succeeds or maximum retries are reached
  until terraform apply -auto-approve
  do
      retry_count=$((retry_count+1))
      if [ $retry_count -ge $max_retries ]; then
          echo " ❌ Terraform apply failed after $retry_count retries."
          exit 1
      fi
      echo " ⚠️ Terraform apply  failed. Retrying in 10 seconds... ($retry_count/$max_retries)"
      sleep 10
  done

  echo " ✅ Terraform apply done after $retry_count retries"
fi
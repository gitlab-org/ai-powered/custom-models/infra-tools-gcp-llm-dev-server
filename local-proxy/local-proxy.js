const http = require('http');
const url = require("url");
const httpProxy = require('http-proxy');

const proxy = httpProxy.createProxyServer({});

const PORT = 8000;

http.createServer(function(req, res) {
    const reqUrl = url.parse(req.url).pathname

    if(reqUrl == "/proxy-health-check") {
        res.write("Proxy is awake")
        res.end()
    }
    else {
        proxy.web(req, res, { target: 'http://localhost:11434', changeOrigin: true });
    }
    
}).listen(PORT);

# Define the project_id variable
variable "project_id" {
  description = "The GCP project ID"
  type = string
  default = "ai-custom-models-dev-7b54d4ec"
}

variable "region" {
  description = "The GCP region"
  type = string
  default = "us-central1"
}

variable "zone_l" {
  description = "The zone letter for deployment"
  type = string
  default = "a"
}

variable "request_collection_id" {
  description = "The Firestore collection ID for requests"
  type        = string
  default     = "ollama-proxy-logs"
}

variable "keys_collection_id" {
  description = "The Firestore collection ID for requests"
  type        = string
  default     = "gcp-servers-keys"
}

variable "request_ttl_field_id" {
  description = "The field ID in Firestore for TTL"
  type        = string
  default     = "created_at"
}

variable "request_ttl" {
  description = "TTL duration in seconds"
  type        = string
  default     = "86400s"  // 24 hours
}

variable "instance_idle" {
  description = "The maximum idle duration in seconds"
  type        = string
  default     = "1800"  // 30 minutes
}

variable "instance_start_duration" {
  description = "Instance startup duration in milli-seconds"
  type        = string
  default     = "90000"  // 1 minutes 30 seconds
}

variable "ollama_port" {
  description = "Ollama server port"
  type        = string
  default     = "8000"
}

variable "cloud_function_runtime" {
  description = "The Version of node running in cloud functions"
  type = string
  default = "nodejs20"
}

# Local Computed Variables
locals {
  zone = "${var.region}-${var.zone_l}"
}

provider "google" {
  project = var.project_id
  region  = var.region
  zone    = local.zone
}

#Some services
resource "google_project_service" "cloudfunctions" {
  service = "cloudfunctions.googleapis.com"
}

resource "google_project_service" "firestore" {
  service = "firestore.googleapis.com"
}

resource "google_project_service" "cloudscheduler" {
  service = "cloudscheduler.googleapis.com"
}

# Reserve a static external IP address
resource "google_compute_address" "static_ip" {
  name = "ollama-endpoint-static-ip"
}

# Output the static IP address
output "static_ip_address" {
  value = google_compute_address.static_ip.address
  description = "The static IP address reserved for the instance."
}

# ML instance to manage
resource "google_service_account" "ollama_vm_sa" {
  account_id   = "ollama-multi-llm-sa"
  display_name = "Custom SA for ollama-multi-llm Instance"
}

resource "google_compute_instance" "app_instance" {
    name         = "ollama-multi-llm"
    machine_type = "a2-ultragpu-1g"
    zone = local.zone

    boot_disk {
        initialize_params {
        image = "deeplearning-platform-release/common-cu121-v20240514-debian-11-py310"
        size = 500
        }
    }

    scheduling {
        on_host_maintenance = "TERMINATE"
    }

    network_interface {
        network = "default"
        access_config {
        nat_ip = google_compute_address.static_ip.address
        }
    }

    tags = ["ml-server"]

    service_account {
      # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
      email  = google_service_account.ollama_vm_sa.email
      scopes = ["cloud-platform"]
    }

    metadata_startup_script = <<-EOF
    # install dependencies
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash
    curl -fsSL https://ollama.com/install.sh | sh
    nvm install 20
    
    # setup ollama
    systemctl enable ollama
    systemctl restart ollama

    ollama pull mistral:instruct
    ollama pull gemma:7b
    ollama pull mixtral:8x22b

    ollama cp mistral:instruct mistral
    ollama rm mistral:instruct
    ollama cp mixtral:8x22b mixtral
    ollama rm mixtral:8x22b
    ollama cp gemma:7b gemma
    ollama rm gemma:7b

    # setup proxy server
    git clone https://gitlab.com/jpcyiza/llm-dev-server-infra-tools.git
    cd llm-dev-server-infra-tools/local-proxy
    npm install
    npm run start:prod
    EOF
}

resource "google_compute_firewall" "default" {
  name    = "proxied-ollama-http"
  network = "default"

  allow {
    protocol = "tcp"
    # opened ollama default port
    ports    = [var.ollama_port]
  }
  source_ranges = ["216.239.36.54/32"]
  target_tags   = ["ml-server"]
}

# Bucket to store Google Cloud Function for allama management
resource "google_storage_bucket" "function_bucket" {
  name     = "${var.project_id}-ollama-function-bucket"
  location = "US"
}

# ollama proxy configurations
## ollama proxy code archive
resource "google_storage_bucket_object" "proxy_zip" {
  name   = "ollama_proxy_v1.zip"
  bucket = google_storage_bucket.function_bucket.name
  source = "functions/proxy.zip"
}

## ollama proxy cloudfunction declaration
resource "google_cloudfunctions_function" "proxy" {
  name                  = "ollama"
  description           = "A function that logs timestamp and proxies requests"
  runtime               = var.cloud_function_runtime
  available_memory_mb   = 128
  source_archive_bucket = google_storage_bucket.function_bucket.name
  source_archive_object = google_storage_bucket_object.proxy_zip.name
  trigger_http          = true
  entry_point           = "handler"

  environment_variables = {
    PROJECT_ID = var.project_id
    TARGET_URL = "${google_compute_address.static_ip.address}:${var.ollama_port}",
    COLLECTION_ID = var.request_collection_id,
    KEYS_COLLECTION_ID = var.keys_collection_id,
    ZONE          = local.zone,
    INSTANCE_NAME = google_compute_instance.app_instance.name
  }
}

# IAM rules for ollama proxy cloudfunction
resource "google_cloudfunctions_function_iam_member" "invoker" {
  project        = google_cloudfunctions_function.proxy.project
  region         = google_cloudfunctions_function.proxy.region
  cloud_function = google_cloudfunctions_function.proxy.name

  role   = "roles/cloudfunctions.invoker"
  # Unauthenticated access allowed for all internet
  member = "allUsers"
}

resource "google_project_iam_member" "proxy_function_compute_admin" {
  project = var.project_id
  # Allow ollama proxy to manage all gc instances as an admin
  role    = "roles/compute.instanceAdmin"
  member  = "serviceAccount:${google_cloudfunctions_function.proxy.service_account_email}"
}

output "proxy_url" {
  description = "The URL of the deployed Cloud Function"
  value       = google_cloudfunctions_function.proxy.https_trigger_url
}

# ollama Shutdown configurations
## ollama Shutdown code archive
resource "google_storage_bucket_object" "shutdown_zip" {
  name   = "shutdown_function.zip"
  bucket = google_storage_bucket.function_bucket.name
  source = "functions/shutdown.zip"
}

## ollama Shutdown cloudfunction declaration
resource "google_cloudfunctions_function" "shutdown" {
  name                  = "ollama-shutdown-function"
  description           = "Shuts down a GCE instance if no requests in last 2 minutes"
  runtime               = var.cloud_function_runtime
  available_memory_mb   = 128
  source_archive_bucket = google_storage_bucket.function_bucket.name
  source_archive_object = google_storage_bucket_object.shutdown_zip.name
  trigger_http          = true
  entry_point           = "checkAndShutdown"

  environment_variables = {
    PROJECT_ID    = var.project_id,
    ZONE          = local.zone,
    INSTANCE_NAME = google_compute_instance.app_instance.name,
    INSTANCE_MAX_IDLE_DURATION = var.instance_idle,
    COLLECTION_ID = var.request_collection_id,
    INSTANCE_START_DURATION = var.instance_start_duration
  }
}

# IAM rules for ollama shutdown function
resource "google_project_iam_member" "shutdown_function_compute_admin" {
  project = var.project_id
  # Allow ollama shutdown function to manage all gc instances as an admin
  role    = "roles/compute.instanceAdmin"
  member  = "serviceAccount:${google_cloudfunctions_function.shutdown.service_account_email}"
}

# scheduler job to shutdown ollama
resource "google_cloud_scheduler_job" "shutdown_scheduler" {
  name             = "ollama-shutdown-scheduler"
  description      = "Schedules the shutdown function"
  schedule         = "*/15 * * * *" // Run every 15 minutes
  time_zone        = "Etc/UTC"
  attempt_deadline = "320s"

  http_target {
    http_method = "POST"
    uri         = google_cloudfunctions_function.shutdown.https_trigger_url
    oidc_token {
      service_account_email = google_service_account.shutdown_scheduler_service_account.email
    }
  }
}

# IAM rules for ollama shutdown scheduler
resource "google_service_account" "shutdown_scheduler_service_account" {
  account_id   = "ollamashutdown-scheduler-sa"
  display_name = "Scheduler Service Account"
}

resource "google_project_iam_member" "shutdown_scheduler_service_account_invoker" {
  project = var.project_id
  # Allow shutdown scheduler to invoke all cloudfunctions
  role    = "roles/cloudfunctions.invoker"
  member  = "serviceAccount:${google_service_account.shutdown_scheduler_service_account.email}"
}

# ollama requests clean configurations
## ollama requests clean code archive
resource "google_storage_bucket_object" "clean_up_zip" {
  name   = "clean_up.zip"
  bucket = google_storage_bucket.function_bucket.name
  source = "functions/clean_up.zip"
}

## ollama requests clean function declaration
resource "google_cloudfunctions_function" "clean_up_function" {
  name                  = "ollama-cleanup-function"
  description           = "Cleans up old request logs, keeping only the latest one"
  runtime               = var.cloud_function_runtime
  available_memory_mb   = 128
  source_archive_bucket = google_storage_bucket.function_bucket.name
  source_archive_object = google_storage_bucket_object.clean_up_zip.name
  trigger_http          = true
  entry_point           = "cleanUpRequests"

  environment_variables = {
    PROJECT_ID = var.project_id,
    COLLECTION_ID = var.request_collection_id
  }
}

# ollama requests cleanup scheduler
resource "google_cloud_scheduler_job" "cleanup_scheduler" {
  name             = "ollama-requests-cleanup-scheduler"
  description      = "Schedules the cleanup function"
  schedule         = "0 0 */3 * *" // Run every 3 days
  time_zone        = "Etc/UTC"
  attempt_deadline = "320s"

  http_target {
    http_method = "POST"
    uri         = google_cloudfunctions_function.clean_up_function.https_trigger_url
    oidc_token {
      service_account_email = google_service_account.cleanup_scheduler_service_account.email
    }
  }

  depends_on = [google_cloudfunctions_function_iam_member.invoker]
}

# IAM rules for ollama requests cleanup scheduler
resource "google_service_account" "cleanup_scheduler_service_account" {
  account_id   = "ollamacleanup-scheduler-sa"
  display_name = "Cleanup Scheduler Service Account"
}

resource "google_project_iam_member" "scheduler_service_account_invoker" {
  project = var.project_id
  # Allow requests cleanup scheduler to invoke all cloudfunctions
  role    = "roles/cloudfunctions.invoker"
  member  = "serviceAccount:${google_service_account.cleanup_scheduler_service_account.email}"
}
